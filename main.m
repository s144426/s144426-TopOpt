% Introduction to Topology Optimisation
% Niels Brummerstedt, DTU, 2019

% Start
close all force, clear variables, clear global
addpath ARCHIVE FUNCTIONS FUNCTIONS/FEA FUNCTIONS/SOLVERS FUNCTIONS/HELPERS ...
    FUNCTIONS/MESHING FUNCTIONS/MESHING/DISTMESH OUTPUT

% Study parameters
study.type          = 'topopt';
study.name          = 'test';
study.problem       = 'MBB';      % MBB, multiload, beam, mechanism
study.domain        = 'box';            % box, hole, skew
study.meshtype      = 'simple';       	% quick (top88), simple (top88ext), noisy, distmesh (triangular), exmesh (quad)
study.optimisation  = 'oc';             % oc, mma
study.filter.type   = 'heaviside';        % none, sensitivity, density, heaviside
study.filter.size   = 10/100;
study.volfrac       = 30/100;
study.simp.start    = 2;
study.simp.iter     = 1;
study.simp.grow     = 1/20;
study.simp.stop     = 3;
study.beta.start    = 0;
study.beta.iter     = 20;
study.beta.grow     = 2;
study.beta.stop     = 256;
study.eta           = 1;
study.stop          = 1/200;
study.iterations    = 500;
study.move          = 0.2;
study.checkfd       = 0;
study.parallel      = false;

% Plots
study.plot.terminal = true;
study.plot.mesh     = true;
study.plot.boundary = true;
study.plot.design   = true;
study.plot.deflect  = true;
study.plot.history  = true;
study.plot.filter   = true;
study.plot.final    = true;
study.plot.finalbc  = false;
study.plot.save     = true;

% Materials
material.E0         = 1;
material.Emin       = 1e-9;
material.nu         = 0.3;
material.model      = 'simp';           % simp, real

% Domain
domain.resolution   = 20;                           % Number of elements on shortest side
study.filter.size   = domain.resolution*study.filter.size;
domain.L            = [2 1]*domain.resolution;      % Lengths
domain              = build_domain(study,domain);   % Problem definition

% Run the analysis
switch study.type
    case 'fea'   , solution = study_fea(    study, domain, material  );
    case 'topopt', solution = study_topopt( study, domain, material  );
end
