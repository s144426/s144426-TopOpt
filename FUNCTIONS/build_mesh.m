function mesh = build_mesh(solution,study,domain,material,mesh)
    
    if mesh.new
        
        fprintf('Computing mesh...\n')
        time = tic;

        switch study.meshtype

            case 'quick'
                [p,t,mesh] = quickmesh(domain);
                mesh = meshaware(p,t,study.filter.size,mesh);

            case 'simple'
                [p,t] = simplemesh(domain);
                mesh = meshaware(p,t,study.filter.size);

            case 'noisy'
                mesh = noisymesh(domain);
                mesh.dofs = numel(mesh.nodes);

            case 'distmesh'
                [p,t] = distmesh(domain);
                mesh = meshaware(p,t,study.filter.size);

            case 'exmesh'
                [p,t] = exmesh(domain);
                mesh = meshaware(p,t,study.filter.size);

            otherwise
                error(['Mesh type ' study.meshtype ' not implemented!'])

        end

        mesh.type = study.meshtype;

        fprintf('\b\b\b\b, %2f s\n',toc(time))

        % Plot mesh
        update_plot(solution,study,domain,mesh,'mesh');

        % Build load and support
        mesh = build_boundary(solution,study,mesh,domain);

        % Build constitutive building blocks
        mesh = build_constitutive(study,mesh,material);

        % Build filter
        mesh = build_filter(solution,study,domain,mesh);

        % Set new mesh to false
        mesh.new = false;
    
    end

end