% SIM Solve
function [x,epsilon,iterations] = numsolve(A,b,method,tolerance,maxiterations,omega)
    
    if nargin < 6; omega = 1; end
    if nargin < 5; maxiterations = 1e5; end
    if nargin < 4; tolerance = 1e-8; end
    if nargin < 3; method = 'mldivide'; end

    switch method
        case { 'mldivide', 'backslash', 'matlab', 'direct' }
            if isstruct(A); error('Incorrect input A, should be a matrix.'); end
            x = mldivide( A , b );
            epsilon = nan;
            iterations = nan;
        
        case { 'LU' }
            if ~isfield(A,{'L','U'}); error('Incorrect input A, should be struct with lower and upper factorised matrices.'); end
            x = A.U\(A.L\b);
            epsilon = nan;
            iterations = nan;
            
        case { 'LUPQR' }
            if ~isfield(A,{'L','U','P','Q','R'}); error('Incorrect input A, should be struct with L, U, P, Q, R factorised matrices.'); end
            x = A.Q*(A.U\(A.L\(A.P*(A.R\b))));
            epsilon = nan;
            iterations = nan;
            
        case { 'SOR', 'sor', 'GaussSeidel', 'Gauss-Seidel', 'GS' }
            if sum(strcmp(method,{'GaussSeidel','Gauss-Seidel','GS'})); omega = 1; end
            if isstruct(A); error('Incorrect input A, should be a matrix.'); end
            [x,epsilon,iterations] = forwardSOR(A,b,omega,maxiterations,tolerance);
            
        case { 'Jacobi', 'jacobi', 'iterate' }
            if isstruct(A); error('Incorrect input A, should be a matrix.'); end
            [x,epsilon,iterations] = Jacobi(A,b,maxiterations,tolerance);
        
        otherwise
            error('I don''t know this SIM method yet.');
            
    end

end

% Jacobi
function [x,epsilon,iterations] = Jacobi(A,b,maxiterations,tolerance,guess)

    % Input arguments
    if nargin < 5; guess = ones(length(b),1); end
    if nargin < 4; tolerance = 1e-6; end
    if nargin < 3; maxiterations = 1e4; end
    
    % First x
    x = guess;
    
    % Calculate error
    r = b - A*x;
    if norm(b) <= eps; epsilon = norm(r); else; epsilon = norm(r)/norm(b); end
    iterating = epsilon > tolerance;
    
    % Build Jacobi matrices
    M = diag(diag(A));
    N = M - A;
    
    % Iterate to solution
    iterations = 0;
    while iterating
        iterations = iterations + 1;
        old = x;
        x = M\(N*x+b);
        epsilon = norm(x-old)/norm(x);
        iterating = ( epsilon > tolerance && iterations < maxiterations );
    end
    
    if iterations == maxiterations
        fprintf('Jacobi returned with max iterations, i=%i, and the error %2.2f.\n',iterations,epsilon); 
    end
    
end

% Forward SOR (Gauss-Seidel if omega = 1)
function [x,epsilon,iterations] = forwardSOR(A,b,omega,maxiterations,tolerance,guess)
    
    % Input arguments
    if nargin < 6; guess = ones(length(b),1); end
    if nargin < 5; tolerance = 1e-6; end
    if nargin < 4; maxiterations = 1e4; end
    if nargin < 3; omega = 1; end
    
    % Error if omega is wrong at this point
    if omega >= 2 || omega <= 0; epsilon('Invalid omega! omega was %d',omega); end
    
    % First x
    x = guess;
    
    % Calculate error
    r = b - A*x;
    if norm(b) <= eps; epsilon = norm(r); else; epsilon = norm(r)/norm(b); end
    iterating = epsilon > tolerance;

    % Build SOR matrices
    M = diag(diag(A)) + omega*tril(A,-1);
    N = (1-omega) * diag(diag(A)) - omega*triu(A,1);
    
    % Iterate to solution
    iterations = 0;
    while iterating
        xo = x; 
        iterations = iterations+1;
        x = M \ ( N*x + omega*b );
        epsilon = norm(x-xo)/norm(x);
        iterating = ( epsilon > tolerance && iterations < maxiterations );
    end
    
    if iterations == maxiterations
        fprintf('SOR returned with max iterations, i=%i, and the error %2.2f.\n',iterations,epsilon); 
    end
    
end

