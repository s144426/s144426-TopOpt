function solution = build_initial(solution,study)

    % Create new solution
    if solution.new
        
        % Set new analysis to false
        solution.simp = study.simp.start;
        solution.beta = study.beta.start;
        
        % Reset history
        solution.history.change     = [];
        solution.history.objective  = [];
        solution.history.maxu       = [];
        
        % Reset loop counters
        solution.loop.iter = 0;
        solution.loop.beta = 0;
        solution.loop.simp = 0;
        solution.loop.mma  = 0;
        solution.change = study.move;
        
        % Set new analysis to false
        solution.new = false;
        
    end

end