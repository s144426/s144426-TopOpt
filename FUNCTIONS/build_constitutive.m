function mesh = build_constitutive(study,mesh,material)

    switch study.meshtype
        case 'quick'
            nelx = mesh.nelx;
            nely = mesh.nely;
            A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
            A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
            B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
            B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
            mesh.KE = 1/(1-material.nu^2)/24*([A11 A12;A12' A11]+material.nu*[B11 B12;B12' B11]);
            nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
            mesh.edofVec = reshape(2*nodenrs(1:end-1,1:end-1)+1,nelx*nely,1);
            mesh.edofMat = repmat(mesh.edofVec,1,8)+repmat([0 1 2*nely+[2 3 0 1] -2 -1],nelx*nely,1);
            mesh.iK = reshape(kron(mesh.edofMat,ones(8,1))',64*nelx*nely,1);
            mesh.jK = reshape(kron(mesh.edofMat,ones(1,8))',64*nelx*nely,1);
            
        otherwise
            
            if study.parallel
                
                % Material parameters
                young = material.E0;
                poisson = material.nu;
                thickness = 1;
                
                element = mesh.element;
                
                parfor e = 1:mesh.ne
                    
                    switch element(e).type
                        case {'quad','plane42'}
                            element(e).k = plane42(element(e).coordinates,young,poisson,thickness);
                        case {'tri','plane32'}
                            element(e).k = plane32(element(e).coordinates,young,poisson,thickness);
                        otherwise
                            error('Unknown element type')
                    end
                    
                end
                mesh.element = element;
                
            else
                
                for element = mesh.element(:)'

                    % Material parameters
                    young = material.E0;
                    poisson = material.nu;
                    thickness = 1;
                    e = element.number;

                    % Local stiffness matrix
                    switch element.type
                        case {'quad','plane42'}
                            k = plane42(element.coordinates,young,poisson,thickness,element.area);
                        case {'tri','plane32'}
                            k = plane32(element.coordinates,young,poisson,thickness);
                        otherwise
                            error('Unknown element type')
                    end
                    mesh.element(e).k = k;

                end
                
            end
            
    end
    
end