function mesh = build_boundary(solution,study,mesh,domain)

    time = tic;
    fprintf('Computing load and support...\n')

    % Find all dofs
    switch study.meshtype
        case 'quick'
            support.alldofs = 1:2*(mesh.nely+1)*(mesh.nelx+1);
        case {'simple','noisy','distmesh','exmesh'}
            support.alldofs = 1:numel(mesh.nodes);
    end

    % Determine number of loadcases
    load.loadcases = numel(unique([domain.BC.loadcase]));

    % Preallocate load and support
    load.F = sparse(numel(mesh.nodes),load.loadcases);
    support.fixeddofs = [];

    % Preallocate mechanism
    if strcmp(study.problem,'mechanism')
        mesh.mechanism.l.out = [];
        mesh.mechanism.l.in = [];
    end

    % Populate load and support
    for BC = domain.BC
        switch BC.variable

            case 'Fx'
                if ~isfield(BC,'loadcase'); BC.loadcase = 1; end
                dofs = find_dofs(study,domain,mesh,BC);
                load.F(dofs,BC.loadcase) = BC.value;

            case 'Fy'
                if ~isfield(BC,'loadcase'); BC.loadcase = 1; end
                dofs = find_dofs(study,domain,mesh,BC);
                load.F(dofs,BC.loadcase) = BC.value;

            case 'u'
                dofs = find_dofs(study,domain,mesh,BC);
                support.fixeddofs = union(support.fixeddofs,dofs);

            case 'v'
                dofs = find_dofs(study,domain,mesh,BC);
                support.fixeddofs = union(support.fixeddofs,dofs);

            case 'spring_out_x'
                dofs = find_dofs(study,domain,mesh,BC);
                mesh.mechanism.l.out = union(mesh.mechanism.l.out,dofs);

            case 'spring_out_y'
                dofs = find_dofs(study,domain,mesh,BC);
                mesh.mechanism.l.out = union(mesh.mechanism.l.out,dofs);

            case 'spring_in_x'
                dofs = find_dofs(study,domain,mesh,BC);
                mesh.mechanism.l.in = union(mesh.mechanism.l.in,dofs);

            case 'spring_in_y'
                dofs = find_dofs(study,domain,mesh,BC);
                mesh.mechanism.l.in = union(mesh.mechanism.l.in,dofs);

            otherwise
                error('Unknown boundary condition')
        end
    end

    % Find free dofs
    support.freedofs = setdiff(support.alldofs,support.fixeddofs);

    % Wrap things up
    mesh.load = load;
    mesh.support = support;

    % Output timing to terminal
    fprintf('\b\b\b\b, %2f s\n',toc(time))

    % Plot load and support
    update_plot(solution,study,domain,mesh,'boundary');

end

function dofs = find_dofs(study,domain,mesh,BC)

    % Check Dijkstra's Algorithm
    
    % Start at a good guess
    guess = (BC.location(1)/domain.L(1) + BC.location(2)/domain.L(2)/domain.L(1) )/2;
    guess = 1/10;
    current = min(mesh.ne,max(1,ceil(mesh.ne*guess)));
    
    % Preallocate arrays
    found.eles = [];
    found.dofs = [];
    found.node = [];
    searched   = [];
    
    % Initialise logicals
    finished   = false;
    found.first = false;
    
    % Loop until finished
    while ~finished
        
        % Current distance
        currentnodes = mesh.element(current).nodes;
        currentminimum = min(BC.position(mesh.nodes(currentnodes,:)));
        
        % Update what we have searched
        searched = unique([searched(:); current]);
        
        % Find new elements in neighborhood
        newneighbors = setdiff(unique([mesh.element(current).neighbours]),searched);
        
        % Add to queue based on position
        queue = newneighbors;
        
        % Remove searched elements from queue
        queue = setdiff(queue(:),searched);
        
        % Node list of queued elements
        nodes = [mesh.element(queue).nodes];
        nodes = unique(nodes(:));
        
        % Position of nodes
        locations = mesh.nodes(nodes,:);
        
        % Evaluate criteria and get a list of attractive nodes
        if ~found.first
            goodnodes = nodes(BC.position(locations) == min(BC.position(locations)));
        else
            goodnodes = nodes(BC.position(locations) < domain.relax);
        end
        
        % Find corresponding attractive elements
        goodelements = queue(logical(sum(ismember(mesh.elements(queue,:),goodnodes),2)));
        
        % Stop if no new elements were found
        if isempty(goodelements)
            finished = true;
        end
        
        % Good nodes minimum
        [goodminimum,goodindex] = min(BC.position(mesh.nodes(goodnodes,:)));
        
        % Build matching nodes
        if goodminimum > currentminimum
            matchnodes = goodnodes(goodindex);
        else
            matchnodes = goodnodes(BC.position(mesh.nodes(goodnodes,:)) < domain.relax);
        end
        
        % If any nodes are matching
        if ~isempty(matchnodes)
            % Calculate matching elements and dofs
            matchelements = queue(logical(sum(ismember(mesh.elements(queue,:),matchnodes),2)));
            matchdofs = matchnodes*2 - logical(sum(strcmp(BC.variable,{'Fx','u','spring_out_x','spring_in_x'})));
            % Toggle first found logical
            found.first = true;
            % Save found nodes, elements, and dofs
            found.eles = unique([found.eles(:);matchelements(:)]);
            found.dofs = unique([found.dofs(:);matchdofs(:)]);
            found.node = unique([found.node(:);matchnodes(:)]);
        end
        
        % Plot the searched elements
        if study.plot.boundary
        	plot_searched(searched,found.eles);
        end
        
        % Update current search to use the good elements
        current = goodelements;
        
    end
    
    if study.plot.save
        title('')
        fig = gcf;
        fig.PaperPositionMode = 'auto';
        fig_pos = fig.PaperPosition;
        fig.PaperSize = [fig_pos(3) fig_pos(4)];
        saveas(fig,['OUTPUT/bcsearch_' num2str(BC.number)],'png'); 
        crop(['OUTPUT/bcsearch_' num2str(BC.number) '.png'])
    end
    
    dofs = found.dofs;

end

function plot_searched(searched,found)
    if nargin < 2; found = []; end
    tmp = findall(0,'Tag','MeshPatch');
    colors = repmat([1 1 1],length(tmp.Faces),1);
    colors(searched,:) = repmat([.8,0,0],length(searched),1);
    colors(found,:) = repmat([0,.8,0],length(found),1);
    set(findall(0,'Tag','MeshPatch'),'FaceVertexCData',colors);
    drawnow;
end