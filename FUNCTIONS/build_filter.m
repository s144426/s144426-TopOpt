
% BUILD_FILTER Builds filter operators for topology optimisation.
% Filter operators are working on the design variable which is represented 
% as a vector. H is a matrix working the design variable. 
% Hs is a vector to be multiplied elementwise on the design variable.

function mesh = build_filter(solution,study,domain,mesh)
        
    fprintf('Computing filter stencil...\n')
    time = tic;

    switch study.meshtype
        case {'quick'}

            iH = ones(mesh.nelx*mesh.nely*(2*(ceil(study.filter.size)-1)+1)^2,1);
            jH = ones(size(iH));
            sH = zeros(size(iH));
            counter = 0;
            for i1 = 1:mesh.nelx
                for j1 = 1:mesh.nely
                    e1 = (i1-1)*mesh.nely+j1;
                    for i2 = max(i1-(ceil(study.filter.size)-1),1):min(i1+(ceil(study.filter.size)-1),mesh.nelx)
                        for j2 = max(j1-(ceil(study.filter.size)-1),1):min(j1+(ceil(study.filter.size)-1),mesh.nely)
                            e2 = (i2-1)*mesh.nely+j2;
                            distance = sqrt((i1-i2)^2+(j1-j2)^2);
                            if distance < study.filter.size
                                counter = counter+1;
                                iH(counter) = e1;
                                jH(counter) = e2;
                                sH(counter) = max(0,study.filter.size-distance);
                            end
                        end
                    end
                end
            end
            mesh.H = sparse(iH,jH,sH);
            mesh.Hs = sum(mesh.H,2);

            update_plot(solution,study,domain,mesh,'filter');

        case {'simple','noisy','distmesh','exmesh'}

            % Preallocate triplets to be able to store data for all
            % elements, where each element have at least three connections.
            iH =  ones(mesh.ne*4,1);
            jH =  ones(mesh.ne*4,1);
            sH = zeros(mesh.ne*4,1);

            % Populate triplets
            wb = waitbar(0,'Working on filter...');
            counter = 0; 
            filterradius = study.filter.size * mean(mesh.radius);
            for element = mesh.element'
                waitbar(element.number/mesh.ne,wb,'Working on filter...');

                for neighbor = mesh.element(element.region)'
                    distance = norm(element.center-neighbor.center);
                    if distance < filterradius
                        counter = counter+1;
                        iH(counter) = element.number;
                        jH(counter) = neighbor.number;
                        sH(counter) = (filterradius-distance)/filterradius;
                    end
                end
            end
            close(wb)
            mesh.H = sparse(iH,jH,sH);
            mesh.Hs = sum(mesh.H,2);

            update_plot(solution,study,domain,mesh,'filter');

        otherwise
           error(['Mesh type ' study.meshtype ' not implemented!'])
    end

    % Output to tefilter.sizeal
    fprintf('\b\b\b\b, %2f s\n',toc(time))
    fprintf([repmat('-',1,48) '\nLooping to convergence...\n'])

end