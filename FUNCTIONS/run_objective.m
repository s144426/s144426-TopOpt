
% RUN_OBJECTIVE Evaluates the objective function and derivative

function solution = run_objective(solution,study,mesh,material)
    switch study.problem
        case {'MBB','multiload','multifail','beam','hole','strange'}
            solution = compliance_minimisation(solution,study,mesh,material);
        case 'mechanism'
            solution = mechanism_synthesis(solution,study,mesh,material);
        otherwise
            error('Unknown problem objective')
    end
    % Update history of objective function
    % Initialised in build_variables
    solution.history.objective = [solution.history.objective solution.c];
end

function solution = compliance_minimisation(solution,study,mesh,material)
    % Load cases
    loadcases = size(solution.U,2);
    % Short naming
    xPhys = solution.x.physical;
    penal = solution.simp;
    Emin = material.Emin;
    E0 = material.E0;
    
    % Compute objective function
    switch study.meshtype
        
        case 'quick'
            
            % Short naming
            edofMat = mesh.edofMat;
            k = mesh.KE;
            nelx = mesh.nelx;
            nely=mesh.nely;
            
            % Preallocate
            ce = 0;
            
            % Update for each load
            for loadcase = 1:loadcases
                Ui = solution.U(:,loadcase);
                ce = ce + reshape(sum(  (Ui(edofMat)*k).*Ui(edofMat)  ,2),nely,nelx);            
            end
            c  =   sum(sum((Emin+xPhys.^penal*(E0-Emin)).*ce));
            dc = - penal*(E0-Emin)*xPhys.^(penal-1).*ce;
            
            if ~isfield(solution,'c0'); solution.c0 = c/10; end
            
            solution.ce = ce/solution.c0;
            solution.c = c/solution.c0;
            solution.dc = dc/solution.c0;
            
        case {'simple','noisy','distmesh','exmesh'}
            
            % Preallocate
            ce = zeros(mesh.ne,loadcases);
            dc = zeros(mesh.ne,1);
            % Update for each load
            for loadcase = 1:loadcases
                Ui = solution.U(:,loadcase);
                for element = mesh.element(:)'
                    ce(element.number,loadcase) = Ui(element.dofs)'*element.k*Ui(element.dofs);
                    dc(element.number) = dc(element.number) - solution.simp*(material.E0-material.Emin)*mesh.element(element.number).area*solution.x.physical(element.number).^(solution.simp-1) * ce(element.number,loadcase);
                end
            end
            % Sum of objective function
            c = sum( sum(ce,2) .* (material.Emin+solution.x.physical.^solution.simp*(material.E0-material.Emin)) );
            
            if ~isfield(solution,'c0'); solution.c0 = c/10; end
            
            solution.ce = ce/solution.c0;
            solution.c = c/solution.c0;
            solution.dc = dc/solution.c0;
            
    end
    if size(mesh.load.F,2) == 1
    if abs( solution.U(:,1)'*mesh.load.F - solution.c*solution.c0 )/abs(solution.c*solution.c0) > 1e-8
        keyboard;
    end
    end
end

function solution = mechanism_synthesis(solution,study,mesh,material)
    % Load cases
    loadcases = size(solution.U,2);
    % Compute objective function
    switch study.meshtype
        case 'quick'
            % Preallocate
            solution.c = 0;
            solution.dc = 0;
            % Update for each load
            for loadcase = 1:loadcases
                Ui = solution.U(:,loadcase);
                solution.ce = reshape(sum((solution.lagrangian(mesh.edofMat)*mesh.KE).*Ui(mesh.edofMat),2),mesh.nely,mesh.nelx);
                solution.c = solution.c + solution.G'*Ui;
                solution.dc = solution.dc + solution.simp*(material.E0-material.Emin)*solution.x.physical.^(solution.simp-1).*solution.ce;
            end
        otherwise
            error(['The mesh type ' study.meshtype ' is not implemented yet!']);
    end
end