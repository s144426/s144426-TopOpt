
% RUN_FILTER Filters variables

function solution = run_filter(solution,study,mesh)

    switch study.filter.type
        case 'none'
            % Nothing to be done
            
        case 'sensitivity'
            solution.dc(:) = mesh.H*(solution.x.design(:).*solution.dc(:)) ./ ...
                                ( mesh.Hs .* max(1e-3,solution.x.design(:)) );
                            
        case 'density'
            solution.dc(:) = filter_linear(solution.dc,mesh.H,mesh.Hs);
            solution.dv(:) = filter_linear(solution.dv,mesh.H,mesh.Hs);
            
        case 'heaviside'
            solution.dx    = filter_heaviside(solution.x.intermediate,solution.beta,study.eta,'derivative');
            solution.dc(:) = filter_linear(solution.dc(:).*solution.dx(:),mesh.H,mesh.Hs);
            solution.dv(:) = filter_linear(solution.dv(:).*solution.dx(:),mesh.H,mesh.Hs);
            
        otherwise
            error(['Filter type "' study.filter '" unknown!']);
            
    end

end

