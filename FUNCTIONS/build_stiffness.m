function mesh = build_stiffness(solution,study,mesh,material)

    % Get current simp variable
    if ~isfield(solution,'simp'); solution.simp = study.simp.start; end

    % Build stiffness matrix
    switch study.meshtype
        
        case {'quick'}
            mesh = build_stiffness_quick(solution,mesh,material);
            
        case {'simple','noisy','distmesh','exmesh'}
            mesh = build_stiffness_matrix(solution,mesh,material);
            
        otherwise
            error(['Mesh type ' study.meshtype ' not implemented yet!'])
            
    end
    
    % In the case of spring stiffness, we need to add artificial stiffness 
    if strcmp(study.problem,'mechanism')
        if isfield(mesh.mechanism.l,'in')
            spring = union(mesh.mechanism.l.out,mesh.mechanism.l.in);
        else
            spring = mesh.mechanism.l.out;
        end
        force = full(mean(mesh.load.F(mesh.mechanism.l.in)))/2;
        for s = spring
            mesh.K(s,s) = mesh.K(s,s) + force;
        end
    end
    
end

function mesh = build_stiffness_quick(solution,mesh,material)

    % Unpack
    nelx    = mesh.nelx;
    nely    = mesh.nely;
    penal   = solution.simp;
    
    % Build
    switch material.model
        case 'simp'
            value = material.Emin+solution.x.physical(:)'.^penal*(material.E0-material.Emin);
        case 'real'
            value = ones(1,mesh.ne);
    end
    
    sK = reshape(mesh.KE(:)*value,64*nelx*nely,1);
    K = sparse(mesh.iK,mesh.jK,sK); 
    mesh.K = (K+K')/2;
    
end

function mesh = build_stiffness_matrix(solution,mesh,material)

    entries= 0;
    for element = mesh.element'
        entries = entries + length(element.dofs)^2;
    end
    I = zeros(entries,1);
    J = I;
    V = I;
    counter = 1;
    
    for element = mesh.element'
        
        % Material parameters
        penal = solution.simp;
        e = element.number;
        
        % Calculate stiffness correction based on density
        switch material.model
            case 'simp'
                value = material.Emin + solution.x.physical(e)  ^penal*(material.E0-material.Emin);
            case 'real'
                value = 1;
        end
        
        % Assemble into global stiffness matrix
        for i = 1:length(element.dofs)
            for j = 1:length(element.dofs)
                I(counter)      = element.dofs(i);
                J(counter)      = element.dofs(j);
                V(counter)      = value*mesh.element(e).k(i,j);
                counter = counter + 1;
            end
        end
        
    end
    K = sparse(I,J,V,mesh.dofs,mesh.dofs);
    mesh.K = (K+K')/2;

end