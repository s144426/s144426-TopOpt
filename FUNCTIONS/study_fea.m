function [solution,study,domain,material] = study_fea(study,domain,material)

    % Print start message
    fprintf([repmat('=',1,48) '\nFinite Element Analysis running...\n' repmat('-',1,48) '\n'])
    
    % Prealloc temp
    solution.temp = 1;
    
    % Build mesh
    [solution,domain] = build_mesh(solution,study,domain);

    % Build missing solution variables
    [solution,domain] = build_variables(solution,study,domain);

    % Build load and support
    [solution,domain] = build_boundary(solution,study,domain);

    % Build stiffness
    [solution,domain] = build_stiffness(solution,study,domain,material);

    % Finite Element Analysis
    solution = run_fea(solution,study,domain);
    
    % Print end message
    fprintf(['\nDone.\n' repmat('=',1,48) '\n'])
        
end