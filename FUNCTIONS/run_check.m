
% RUN_CHECK Tests the calculated sensitivities with finite differences

function solution = run_check(solution,study,domain,mesh,material)
    
    if study.checkfd && mod(solution.loop.iter,study.checkfd) == 0

        fprintf('Evaluating sensitivity with FD...\n');
        
        % Select random elements
        es = ceil(rand(1,1)*mesh.ne);
        
        % Define perturbations
        hs = 10.^(-6:0.25:1);
        
        % Counter
        ie = 0;
        
        % Prealloc
        error = zeros(length(es),length(hs));
        
        % Loop through elements
        wb = waitbar(0,'Working on FD check...');
        for e = es
            
            
            % Counter
            ie = ie+1;
            ih = 0;
            
            % Loop through perturbations
            for h = hs
                
                % Counter
                ih = ih+1;
                
                percentage = (ie-1)/length(es) + (ih/length(hs))/length(es);
                waitbar(percentage,wb,'Working on FD check...');

                % Object function values
                obj_0 = get_perturbed_c(solution,study,domain,mesh,material,e,0);
                obj_1 = get_perturbed_c(solution,study,domain,mesh,material,e,h);

                % Compare
                sens_adj = solution.dc(e);
                sens_fd = (obj_1-obj_0)/h;
                error(ie,ih) = abs(sens_fd-sens_adj)/abs(sens_adj);
                
                % Print
                % fprintf('e: %5i, h: %1e, fd: %.3e, adj: %.3e, err: %.3e\n',e,h,sens_fd,sens_adj,error(ie,ih))
                
            end
             
        end
        close(wb)
        
        % Make figure
        update_plot(solution,study,domain,mesh,'fd',{hs,error});
        
        M = [hs' error'];
        dlmwrite('OUTPUT/fd_check.txt',M,'\t');
        
    end

end

function c = get_perturbed_c(solution,study,domain,mesh,material,e,h)

    % Add perturbation to single element
    solution.x.physical(e) = solution.x.physical(e) + h;

    % Build stiffness matrix
    mesh = build_stiffness(solution,study,mesh,material);

    % Run a finite element analysis on the perturbed mesh
    solution = run_fea(solution,study,domain,mesh);

    % Run evaluation on objective function
    solution = run_objective(solution,study,mesh,material);
    
    % Return object function
    c = solution.c;

end