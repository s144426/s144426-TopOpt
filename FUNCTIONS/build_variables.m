function solution = build_variables(solution,study,mesh)
    
    if ~isfield(solution,'x')
        
        % Build design variable
        switch mesh.type
            case {'quick'}
                solution.x.design = repmat(study.volfrac,mesh.nely,mesh.nelx);
            case {'simple','noisy','distmesh','exmesh'}
                solution.x.design = study.volfrac*ones(mesh.ne,1);
        end

        % Build physical and intermediate design variable
        switch study.filter.type
            case {'none','density','sensitivity'}
                solution.x.physical = solution.x.design;
            case {'heaviside'}
                solution.x.intermediate = solution.x.design;
                solution.x.physical = 1 - ...
                    exp(-solution.beta*solution.x.intermediate) + ...
                    solution.x.intermediate*exp(-solution.beta);
        end

        % Build deflection
        solution.U = zeros(numel(mesh.nodes),mesh.load.loadcases);

        % Build mma info
        if strcmp(study.optimisation,'mma')
            solution.mma.constraints = 1; % m
            solution.mma.x      =  nan(size(solution.x.design(:)));
            solution.mma.xold1  =  nan(size(solution.x.design(:)));
            solution.mma.xold2  =  nan(size(solution.x.design(:)));
            solution.mma.lower  =  nan(size(solution.x.design(:)));
            solution.mma.upper  =  nan(size(solution.x.design(:)));
            solution.mma.n      =  length(solution.x.design(:));
            solution.mma.a0     = 1;
            solution.mma.a      = 0;
            solution.mma.c      = 1000;
            solution.mma.d      = 0;
            switch study.problem
                case 'mechanism'
                    solution.mma.asyinit = 0.2;
                    solution.mma.asyincr = 1.05;
                    solution.mma.asydecr = 0.65;
                otherwise
                    solution.mma.asyinit = 0.5;
                    solution.mma.asyincr = 1.2;
                    solution.mma.asydecr = 0.7;
            end
        end
        
    end

end