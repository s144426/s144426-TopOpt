function [p,t,mesh] = quickmesh(domain)

    % Bould domain
    mesh.nelx = domain.L(1)/(min(domain.L)/domain.resolution);
    mesh.nely = domain.L(2)/(min(domain.L)/domain.resolution);
    mesh.dofs = 2*(mesh.nely+1)*(mesh.nelx+1);
    
    % Build p and t arrays
    [x,y]=meshgrid(0:domain.L(1)/mesh.nelx:domain.L(1),flip(0:domain.L(2)/mesh.nely:domain.L(2)));
    p=[x(:),y(:)];
    row = reshape(repmat((1:mesh.nely)',1,mesh.nelx),[],1)-1;
    col = reshape(repmat((1:mesh.nelx) ,mesh.nely,1),[],1)-1;
    t = (col+[0 1 1 0])*(mesh.nely+1)+[2 2 1 1]+row;
    
end