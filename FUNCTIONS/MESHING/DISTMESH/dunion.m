
% DUNION selects the minimum distance
% Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.
% Modified by Niels Brummerstedt, 2019

% Input varargin containing n column vectors of number of points p distances

function d=dunion(varargin)
    for i = 1:length(varargin); varargin{i} = varargin{i}(:); end
    d=min(cell2mat(varargin),[],2);
end