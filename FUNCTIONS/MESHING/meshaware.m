
% MESHAWARE converts a point-connectivity set to a mesh structure
% Input:
%   p       point locations with each row corresponding to a node
%   t       connectivity of point indices, each row is an element
%   c       center coordinate for each element
%   r       length scale ("radius") for each element 
%   rmin    radius for elements in region of an element

function mesh = meshaware(p,t,filtersize,mesh)

    if nargin < 4; mesh = struct; end

    fprintf('\b\b\b\b, adding meshaware...\n')
    
    typenames = {'-','-','plane32','plane42'};
    
    mesh.nodes = p;
    mesh.elements = t;
    mesh.nn = size(p,1);
    mesh.ne = size(t,1);
    mesh.filter.size = filtersize;
    mesh.dofs = numel(mesh.nodes);
    
    mesh.center = zeros(mesh.ne,2);
    for e = 1:mesh.ne
        mesh.center(e,:) = mean(p(t(e,:),:));
    end
    
    for e = 1:mesh.ne
        
        mesh.element(e,1).number       = e;
        mesh.element(e,1).nodes        = mesh.elements(e,:)';
        mesh.element(e,1).dofs         = reshape([mesh.element(e,1).nodes*2-1 mesh.element(e,1).nodes*2]',[],1);
        mesh.element(e,1).coordinates  = mesh.nodes(mesh.element(e,1).nodes,:);
        mesh.element(e,1).type         = typenames{length(mesh.element(e,1).nodes)};
        mesh.element(e,1).neighbours	 = find(sum(ismember(mesh.elements,mesh.elements(e,:)),2)>0)';
        
        % Area
        n = [1:size(mesh.element(e,1).coordinates,1) 1];
        x = mesh.element(e,1).coordinates(:,1);
        y = mesh.element(e,1).coordinates(:,2);
        termspos = x(n(1:end-1)).*y(n(2:end));
        termsneg = y(n(1:end-1)).*x(n(2:end));
        mesh.element(e,1).area = 1/2*abs(sum(termspos)-sum(termsneg));
        
        % Center
        mesh.element(e,1).center = mesh.center(e,:);
        
        % Radius
        mesh.element(e,1).radius = sqrt(mesh.element(e,1).area);
        
        % Region
        mesh.element(e,1).region = find(vecnorm((mesh.center-mesh.element(e,1).center)')' < mesh.element(e,1).radius*filtersize );
            
    end
    
    mesh.areas = [mesh.element.area]';
    mesh.radius = [mesh.element.radius]';
    
    
end