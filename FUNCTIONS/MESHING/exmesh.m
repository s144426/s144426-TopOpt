
%   EXMESH 2-D Mesh Generator using Distance Functions.
%   Heavily modified version of DISTMESH2D
%   By Niels Brummerstedt, June 2019
%   Original Copyright (C) 2004-2012 Per-Olof Persson. (DISTMESH2D)

function [p,t] = exmesh(domain,varargin)

    fd = domain.fd;
    fh = domain.fh;
    h0 = max(domain.L)/domain.resolution/2;
    bbox = domain.box;
    pfix = domain.fix;
    
    % Use orriginal distmesh2d
    [p,t] = distmesh2d(fd,fh,h0,bbox,pfix);
    
    % Reshape mesh to quads
    tt = nan(size(t,1)*3,4);
    for e = 1:size(t,1)
        % How many points did we start with?
        oldnumber = size(p,1); 
        % For each edge, add new edge center if applicable
        position = [];
        new = zeros(1,4);
        existing = [t(e,:) t(e,1)];
        iter = 0;
        for edgenumber = 1:3
            location = (p(existing(0+edgenumber),:)+p(existing(1+edgenumber),:))/2;
            [eval,loc] = ismember(location,p,'rows');
            if ~eval
                iter = iter+1;
                position = [position; location];
                new(edgenumber) = oldnumber+iter;
            else
                new(edgenumber) = loc;
            end
        end
        
        % Add center point
        new(4) = oldnumber+iter+1;
        position = [position; (p(t(e,1),:)+p(t(e,2),:)+p(t(e,3),:))/3];
        
        % Old number of points and additional
        p = [p; position];
        
        % Populate
        tt((e-1)*3+1,:) = [existing(1) new(1) new(4) new(3)];
        tt((e-1)*3+2,:) = [new(1) existing(2) new(2) new(4)];
        tt((e-1)*3+3,:) = [new(4) new(2) existing(3) new(3)];
        
    end
    t = tt;

end
