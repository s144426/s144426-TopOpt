
%   DISTMESH2D 2-D Mesh Generator using Distance Functions.
%   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.
%   Modified June 2019 by Niels Brummerstedt

function [p,t,c,r] = distmesh(domain)

    fd = domain.fd;
    fh = domain.fh;
    h0 = max(domain.L)/domain.resolution/2;
    bbox = domain.box;
    pfix = domain.fix;

    % Run original dismesh2d
    [p,t] = distmesh2d(fd,fh,h0,bbox,pfix);
    
    % Add extra info
    [c,r] = circumcenter(p,t);
    
end