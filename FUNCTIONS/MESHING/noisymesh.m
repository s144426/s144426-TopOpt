function [p,t]=noisymesh(domain)

    domain.nelx = domain.L(1)/(min(domain.L)/domain.resolution);
    domain.nely = domain.L(2)/(min(domain.L)/domain.resolution);
    
    % Build p and t arrays
    [x,y]=meshgrid(0:domain.L(1)/domain.nelx:domain.L(1),flip(0:domain.L(2)/domain.nely:domain.L(2)));
    p=[x(:),y(:)];
    
    % Add location noise
    d=min(domain.L)/domain.resolution;
    p = p+d*0.2*(rand(size(p))*2-1);
    
    % Build elements
    row = reshape(repmat((1:domain.nely)',1,domain.nelx),[],1)-1;
    col = reshape(repmat((1:domain.nelx) ,domain.nely,1),[],1)-1;
    t = (col+[0 1 1 0])*(domain.nely+1)+[2 2 1 1]+row;
    
end