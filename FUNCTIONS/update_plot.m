function update_plot(solution,study,domain,mesh,plottype,inputdata)
    
    % input
    if nargin < 5; inputdata = false; end
        
    % Create plot data
    switch plottype
        case 'design'
            if study.plot.design
                if isempty(findall(0,'type','patch','Tag','DesignPatch'))
                    if isempty(findall(0,'Name','Design'))
                        figure('Name','Design','units','normalized','outerposition',[0.55 0.1 0.4 0.8]); 
                    else
                        figure(findall(0,'Name','Design'))
                    end
                    subplot(2,1,1)
                    patch(...
                        'Tag','DesignPatch',...
                        'Faces',mesh.elements,...
                        'Vertices',mesh.nodes,...
                        'EdgeColor','none',...
                        'FaceColor','k',...
                        'FaceAlpha','flat',...
                        'FaceVertexAlphaData',solution.x.physical(:)...
                    ); axis equal; axis off; colormap gray;
                else
                    set(findall(0,'Tag','DesignPatch'),'FaceVertexAlphaData',solution.x.physical(:));
                end
            end
            
        case 'deflection'
            if study.plot.deflect
                tmp = findall(0,'Tag','DesignPatch');
                if ~isempty(tmp)
                    if isempty(tmp.UserData)
                        tmp.UserData.scale = max(abs(solution.U))/(max(domain.L)/8); 
                    end
                    perturb = reshape(solution.U(:)',2,[])'/tmp.UserData.scale;
                    set(findall(0,'Tag','DesignPatch'),'Vertices',mesh.nodes+perturb);
                end
            end
            
        case 'mesh'
            if study.plot.mesh
                if isempty(findall(0,'type','patch','Tag','MeshPatch'))
                    % Create figure if not there
                    if isempty(findall(0,'Name','Mesh'))
                        figure('Name','Mesh','units','normalized','outerposition',[0.05 0.1 0.4 0.8]); 
                    else
                        figure(findall(0,'Name','Mesh'))
                    end
                    % Create plot
                    subplot(2,1,1)
                    patch(...
                        'Tag','MeshPatch',...
                        'Faces',mesh.elements,...
                        'Vertices',mesh.nodes,...
                        'FaceColor','flat',...
                        'FaceVertexCData',repmat([1 1 1],length(mesh.elements),1)...
                    ); axis equal; axis off; title('Mesh');
                    if domain.resolution < 7
                        p = mesh.nodes;
                        c = mesh.center;
                        hold on
                        for n = 1:size(p,1)
                            text(p(n,1),p(n,2),[' ' num2str(n) ' '],'VerticalAlignment', 'bottom')
                        end
                        for e = 1:mesh.ne
                            text(c(e,1),c(e,2),num2str(e),'VerticalAlignment','middle','HorizontalAlignment','center')
                        end
                        hold off
                    end
                else
                    set(findall(0,'type','patch','Tag','MeshPatch'),'Faces',mesh.elements,'Vertices',mesh.nodes);
                end
            end
            
        case 'boundary'            
            if study.plot.boundary
                % Create or select figure
                if isempty(findall(0,'Name','Mesh'))
                    figure('Name','Mesh','units','normalized','outerposition',[0.05 0.1 0.4 0.8]); 
                else
                    figure(findall(0,'Name','Mesh'))
                end
                % Clear mesh
                set(findall(0,'type','patch','Tag','MeshPatch'),'FaceVertexCData',repmat([1 1 1],length(mesh.elements),1));
                % Create plot data
                subplot(2,1,1)
                hold on
                % Supports
                marks = {'>','^'};
                for support = mesh.support.fixeddofs'
                    node = ceil(support/2);
                    dir = 1-mod(support,2)+1;
                    scatter(mesh.nodes(node,1),mesh.nodes(node,2),75,['k' marks{dir}],'filled');
                end
                % Loads
                marks = {'<','>','v','^'};
                [rows,~,vals] = find(mesh.load.F);
                for index = 1:length(rows)
                    load = rows(index);
                    node = ceil(load/2);
                    dir = (1-mod(load,2))*2; % x = 0 y = 2
                    val = (sign(vals(index))+1)/2; % Positive = 1, Negative = 0
                    ind = dir+val+1;
                    for n = 1:length(node)
                        scatter(mesh.nodes(node(n),1),mesh.nodes(node(n),2),75,['b' marks{ind(n)}],'filled');
                    end
                end
                hold off
            end
            
        case 'history'
            if study.plot.history
                if solution.loop.iter > 1
                    
                    % Objective function history
                    if isempty(findall(0,'Tag','HistoryObj'))
                        % Create figure if not there
                        if isempty(findall(0,'Name','Design'))
                            figure('Name','Design','units','normalized','outerposition',[0.55 0.1 0.4 0.8]); 
                        else
                            figure(findall(0,'Name','Design'))
                        end
                        % Create plot
                        subplot(4,1,3)
                        plot(1:solution.loop.iter',solution.history.objective','Tag','HistoryObj');
                        title('Objective function history');
                        grid on
                    else
                        tmp = findall(0,'Tag','HistoryObj');
                        set(tmp,'XData',1:solution.loop.iter,'YData',solution.history.objective);
                    end
                    % Change
                    if isempty(findall(0,'Tag','HistoryChange'))
                        % Create figure if not there
                        if isempty(findall(0,'Name','Design'))
                            figure('Name','Design','units','normalized','outerposition',[0.55 0.1 0.4 0.8]); 
                        else
                            figure(findall(0,'Name','Design'))
                        end
                        % Create plot
                        subplot(4,1,4)
                        semilogy(1:solution.loop.iter,solution.history.change,'Tag','HistoryChange');
                        title('Change history');
                        grid on
                        ylim([study.stop study.move])
                    else
                        tmp = findall(0,'Tag','HistoryChange');
                        set(tmp,'XData',1:solution.loop.iter,'YData',solution.history.change);
                    end
                end
            end
            
        case 'filter'
            if study.plot.filter
                % Data
                colors = repmat([1 1 1],mesh.ne,1);
                e = floor( mesh.ne*rand(1,1));
                infilter = find(mesh.H(e,:));
                vals = full(mesh.H(e,infilter));
                vals = vals/max(abs(vals));
                colors(infilter,:) = (1-vals)'*[1 1 0] + (1-0*vals)'*[0 0 1];
                % Handle
                MeshPatch = findall(0,'Tag','MeshPatch');
                if isempty(MeshPatch)
                    % Do nothing
                else
                    set(findall(0,'Tag','MeshPatch'),'FaceVertexCData',colors);
                end
                
                
                % SPY
                % Create figure if not there
                if isempty(findall(0,'Name','Mesh'))
                    figure('Name','Mesh','units','normalized','outerposition',[0.05 0.1 0.4 0.8]); 
                else
                    figure(findall(0,'Name','Mesh'))
                end
                subplot(2,2,3)
                spy(mesh.H);
                title('Filter');
                if study.plot.save
                    temp = figure;
                    spy(mesh.H);
                    fig = gca;
                    title('')
                    fig = fig.Parent;
                    fig.PaperPositionMode = 'auto';
                    fig_pos = fig.PaperPosition;
                    fig.PaperSize = [fig_pos(3) fig_pos(4)];
                    saveas(fig,['OUTPUT/' study.name '_filterspy' ],'png'); 
                    crop(['OUTPUT/' study.name '_filterspy.png'])
                    close(temp)
                end
            end
            
        case 'fd'
            
            if iscell(inputdata)
                tmp = findall(0,'Tag','FD');
                hs = inputdata{1};
                error = inputdata{2};
                
                if isempty(tmp)
                    
                    if isempty(findall(0,'Name','Mesh'))
                        figure('Name','Mesh','units','normalized','outerposition',[0.05 0.1 0.4 0.8]); 
                    else
                        figure(findall(0,'Name','Mesh'))
                    end
                    
                    subplot(2,2,4)
                    loglog(hs,error)
                    title('Finite difference check');
                    xlabel('Density perturbation')
                    ylabel('Adjoint sensitivity error')
                    axis equal
                    grid on
                    set(gca,'YMinorGrid','off')
                    set(gca,'XMinorGrid','off')
                    
                else
                    set(tmp,'XData',hs,'YData',error);
                end
                
                
                
            end
            
            
        case 'final'
            
            if study.plot.final
            
                % Select figure to work on
                finalfig = figure('Name','Final design','units','normalized','outerposition',[0.2 0.2 0.64 0.6]);

                % Create plot data
                patch(...
                    'Faces',mesh.elements,...
                    'Vertices',mesh.nodes,...
                    'EdgeColor','none',...
                    'FaceColor','k',...
                    'FaceAlpha','flat',...
                    'FaceVertexAlphaData',solution.x.physical(:)...
                ); 
                axis equal; 
                axis off; 
                colormap gray;
                
                if study.plot.finalbc
                % Create plot data
                hold on
                % Supports
                marks = {'>','^'};
                for support = domain.support.fixeddofs'
                    node = ceil(support/2);
                    dir = 1-mod(support,2)+1;
                    scatter(mesh.nodes(node,1),mesh.nodes(node,2),75,['k' marks{dir}],'filled');
                end
                % Loads
                marks = {'<','>','v','^'};
                [rows,~,vals] = find(domain.load.F);
                for index = 1:length(rows)
                    load = rows(index);
                    node = ceil(load/2);
                    dir = (1-mod(load,2))*2; % x = 0 y = 2
                    val = (sign(vals(index))+1)/2; % Positive = 1, Negative = 0
                    ind = dir+val+1;
                    for n = 1:length(node)
                        scatter(mesh.nodes(node(n),1),mesh.nodes(node(n),2),75,['b' marks{ind(n)}],'filled');
                    end
                end
                hold off
                end
                
                fixfigure;
            
            end
            
            if study.plot.save
                fig = gcf;
                fig.PaperPositionMode = 'auto';
                fig_pos = fig.PaperPosition;
                fig.PaperSize = [fig_pos(3) fig_pos(4)];
                saveas(fig,['OUTPUT/' study.name '_design' ],'png'); 
                crop(['OUTPUT/' study.name '_design.png'])
                close(finalfig)
            end
            
    end
    
    % Draw now
    drawnow;
    
    

end

