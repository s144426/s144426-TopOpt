function distance = distance_line(points,vertex_1,vertex_2)

    % Handle inputt
    if nargin < 3 && min(size(vertex_1)) > 1
        % assume that line coordinates are given as row in matrix
        vertex_2 = vertex_1(2,:);
        vertex_1 = vertex_1(1,:);
    end
    % Preallocate logical array
    distance = zeros(size(points,1),1);
    
    % Convert vertex to 3D coordinates
    vertex_1 = padarray(vertex_1(:),[3-length(vertex_1) 0],0,'post');
    vertex_2 = padarray(vertex_2(:),[3-length(vertex_2) 0],0,'post');
    
    % Create vector between vertices
    a = (vertex_1 - vertex_2);
    
    % Go through all points
    % (Might be good to vectorise) 
    for i = 1:size(points,1)
        % Create row vector with point
        point = padarray(points(i,:),[0 3-length(points(i,:))],0,'post');
        b = point(:) - vertex_2;
        distance(i) = norm(cross(a,b)) / norm(a);
    end
    
end