function answer = is_point_on_line(points,vertex_1,vertex_2,relax)

    % Input handle
    if nargin < 4; relax = 1e-4; end
    
    % Preallocate logical array
    answer = false(size(points,1),1);
    
    % Convert vertex to 3D coordinates
    vertex_1 = padarray(vertex_1(:),[3-length(vertex_1) 0],0,'post');
    vertex_2 = padarray(vertex_2(:),[3-length(vertex_2) 0],0,'post');
    
    % Create vector between vertices
    a = (vertex_1 - vertex_2);
    
    % Go through all points
    % (Might be good to vectorise) 
    for i = 1:size(points,1)
        % Create row vector with point
        point = padarray(points(i,:),[0 3-length(points(i,:))],0,'post');
        b = point(:) - vertex_2;
        answer(i) = norm(cross(a,b)) / norm(a) < relax;
    end
    
end