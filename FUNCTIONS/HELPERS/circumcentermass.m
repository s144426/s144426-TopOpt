function [centers,radii] = circumcentermass(node,element)

    %   Copyright (C) 2004-2012 Per-Olof Persson. See COPYRIGHT.TXT for details.

    elements=size(element,1);
    nodes = size(element,2);
    centers=zeros(elements,2);
    radii=zeros(elements,1);

    for e = 1:elements
        
        x = node(element(e,:),1);
        y = node(element(e,:),2);

        A = 1/2 * (sum(x.*circshift(y,-1))-sum(circshift(x,-1).*y));

        Cx = 1/6/A * sum(  ...
                (x+circshift(x,-1)).*...
                (x.*circshift(y,-1)-circshift(x,-1).*y) ...
            );
        Cy = 1/6/A * sum(  ...
                (y+circshift(y,-1)).*...
                (x.*circshift(y,-1)-circshift(x,-1).*y) ...
            );
        
        centers(e,:) = [Cx Cy];
        radii(e) = sqrt(A);
        
    end

    


end