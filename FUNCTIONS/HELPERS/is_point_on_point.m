function answer = is_point_on_point(points,fixedpoint,relax)
    if nargin < 3; relax = 1e-4; end
    answer = logical( vecnorm(points - fixedpoint,2,2) < relax );
end