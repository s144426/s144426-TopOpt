function distance = sdrectangle(point,x1,x2,y1,y2,domain)


    % Finds the scaled distance from a rectangle in a domain.
    
    corners = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
    scaling = max(abs(-min(min(min(-y1+corners(:,2),y2-corners(:,2)),-x1+corners(:,1)),x2-corners(:,1))));


    distance= ( -min(min(min(-y1+point(:,2),y2-point(:,2)),-x1+point(:,1)),x2-point(:,1)) ) ./ scaling;

    
end