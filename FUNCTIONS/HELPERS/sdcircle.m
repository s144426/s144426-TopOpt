function distance = sdcircle(point,xc,yc,radius,domain)

    % Finds the scaled distance from a circle in a domain.
    
    corners = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
    scaling = max(abs(sqrt((corners(:,1)-xc).^2+(corners(:,2)-yc).^2)-radius));
    
    distance = ( sqrt((point(:,1)-xc).^2+(point(:,2)-yc).^2)-radius )./scaling;
    
end