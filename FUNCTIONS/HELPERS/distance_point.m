function distance = distance_point(points,fixedpoint)
    distance = vecnorm(points-fixedpoint,2,2);
end