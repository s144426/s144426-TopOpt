function answer = is_point_on_circle(pts,xc,yc,r)
    d = sqrt((pts(:,1)-xc).^2+(pts(:,2)-yc).^2)-r < 1e-4;
    if d; answer=true;else;answer=false;end
end