
% RUN_CONSTRAINTS Evaluates constraint functions within an iteration

function solution = run_constraints(solution,study,mesh)

    switch study.meshtype

        case 'quick'
            solution.v = mean(solution.x.physical(:))/study.volfrac - 1;
            solution.dv = (1/study.volfrac) * (1/mesh.ne) * ones(mesh.nely,mesh.nelx);
            
        case {'simple','noisy','distmesh','exmesh'}
            localvolume = solution.x.physical(:).*mesh.areas(:);
            totalvolume = study.volfrac*sum(mesh.areas(:));
            solution.v = sum(localvolume)/totalvolume - 1;
            solution.dv = (1/study.volfrac) * (1/mesh.ne) * ones(mesh.ne,1);
            
    end

end