function domain = update_bc(domain,variable,value,bctype,location,loadcase)

    failure = false;
    if isfield(domain,'BC'); n = length(domain.BC)+1; else; n = 1; end
    if nargin < 6; loadcase = 1; end
    
    switch bctype
        case 'point'
            temp.position = @(pt) distance_point( pt, location );
        case 'line'
            temp.position = @(pt) distance_line( pt, location );
        otherwise
            keyboard
            failure = true;
    end
    temp.variable   = variable;
    temp.value      = value;
    temp.number     = n;
    temp.location   = location;
    if exist('loadcase','var')
        temp.loadcase   = loadcase;
    end
    if ~failure
        domain.BC(n) = temp; 
    end

end

