function k = plane32(coordinates,young,poisson,thickness)
    fact = young/(1-poisson^2);
    C = [ fact fact*poisson 0 ; fact*poisson fact 0 ; 0 0 fact*(1-poisson)/2 ];
    
    [B,detjac] = shape(coordinates);
    k = thickness*detjac/2*B'*C*B;
    
end

function [B,detjac] = shape(coordinates)
    
    x = reshape(coordinates(1:2:end),length(coordinates(1:2:end)),1);
    y = reshape(coordinates(2:2:end),length(coordinates(2:2:end)),1);
    
    detjac = ( x(1)-x(3) )*( y(2)-y(3) ) - ( y(1)-y(3) )*( x(2)-x(3) );
    
    B = 1/detjac * [...
        y(2)-y(3) 0 y(3)-y(1) 0 y(1)-y(2) 0;...
        0 x(3)-x(2) 0 x(1)-x(3) 0 x(2)-x(1);...
        x(3)-x(2) y(2)-y(3) x(1)-x(3) y(3)-y(1) x(2)-x(1) y(1)-y(2) ];
    

end