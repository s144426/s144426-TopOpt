function k = plane42(coordinates,young,poisson,thickness,area)

    if nargin < 5; area = 0; end
    
    coordinates = coordinates';
    coordinates = coordinates(:);
    
    fact = young/(1.-poisson^2);
    C = [ fact fact*poisson 0 ; fact*poisson fact 0 ; 0 0 fact*(1.-poisson)/2 ];
    k = zeros(8,8);
    ng = 2;
    gp(1) =  1/sqrt(3.);
    gp(2) = -1/sqrt(3.);
    W(1) = 1;
    W(2) = 1;
    detjacsum = 0;
    for i = 1:ng
        for j = 1:ng
            xi  = gp(j);
            eta = gp(i);
            [B,~,~,detjac] = shape(coordinates,xi,eta);
            k = k + W(i)*W(j)*thickness*detjac*B'*C*B;
            detjacsum = detjacsum + detjac;
            if detjac < 0; keyboard; end
        end
    end
    
    % Check the determinant to the jacobian
    if area; if abs( detjacsum/area - 1 ) > 0.1; fprintf('%f\n',detjacsum/area); keyboard; end; end
    
end

function [B,N,jac,detjac,Nt,jacinv] = shape(coordinates,xi,eta)
    
    % Reshape coordinates
    coordinates = coordinates';
    coordinates = coordinates(:);
    
    % Initialize matrices
    N       = zeros(2,8);
    jac     = zeros(2,2);
    L       = zeros(3,4);
    Nt      = zeros(4,8);
    jacinv  = zeros(2,2);
    jacinvt = zeros(4,4);
    % Auxillary data
    L(1,1) = 1; 
    L(2,4) = 1; 
    L(3,2) = 1; 
    L(3,3) = 1;
    n1    = (1./4.)*(1-xi)*(1-eta);
    n1xi  = (1./4.)*(-1)*(1-eta);
    n1eta = (1./4.)*(-1)*(1-xi);
    n2    = (1./4.)*(1+xi)*(1-eta);
    n2xi  = (1./4.)*(1)*(1-eta);
    n2eta = (1./4.)*(-1)*(1+xi);
    n3    = (1./4.)*(1+xi)*(1+eta);
    n3xi  = (1./4.)*(1)*(1+eta);
    n3eta = (1./4.)*(1)*(1+xi);
    n4    = (1./4.)*(1-xi)*(1+eta);
    n4xi  = (1./4.)*(-1)*(1+eta);
    n4eta = (1./4.)*(1)*(1-xi);
    % N matrix
    N(1,1) = n1;
    N(1,3) = n2;
    N(1,5) = n3;
    N(1,7) = n4;
    N(2,2) = n1;
    N(2,4) = n2;
    N(2,6) = n3;
    N(2,8) = n4;
    % Extended N matrix
    Nt(1,1) = n1xi; Nt(2,1) = n1eta; Nt(3,2) = n1xi; Nt(4,2) = n1eta;
    Nt(1,3) = n2xi; Nt(2,3) = n2eta; Nt(3,4) = n2xi; Nt(4,4) = n2eta;
    Nt(1,5) = n3xi; Nt(2,5) = n3eta; Nt(3,6) = n3xi; Nt(4,6) = n3eta;
    Nt(1,7) = n4xi; Nt(2,7) = n4eta; Nt(3,8) = n4xi; Nt(4,8) = n4eta;
    % Jacobian matrix
    jac(1,1) = n1xi* coordinates(1) + n2xi* coordinates(3) + n3xi* coordinates(5) + n4xi*coordinates(7);
    jac(1,2) = n1xi* coordinates(2) + n2xi* coordinates(4) + n3xi* coordinates(6) + n4xi*coordinates(8);
    jac(2,1) = n1eta*coordinates(1) + n2eta*coordinates(3) + n3eta*coordinates(5) + n4eta*coordinates(7);
    jac(2,2) = n1eta*coordinates(2) + n2eta*coordinates(4) + n3eta*coordinates(6) + n4eta*coordinates(8);
    % Determinant to jacobian matrix
    detjac = jac(1,1)*jac(2,2)-jac(2,1)*jac(1,2);
    % Inverse to jacobian matrix
    jacinv(1,1) = jac(2,2);
    jacinv(2,2) = jac(1,1);
    jacinv(1,2) = -1.*jac(1,2);
    jacinv(2,1) = -1.*jac(2,1);
    jacinv = (1./detjac) * jacinv;
    % Extended inverse jacobian matrix
    jacinvt(1:2,1:2) = jacinv;
    jacinvt(3:4,3:4) = jacinv;
    % B matrix
    B = L*jacinvt*Nt;
end