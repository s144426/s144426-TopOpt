function x_tilde = filter_heaviside(x,beta,eta,order)

    % Handle input
    if nargin < 4; order = 'zero'; end
    if nargin < 3; eta = 0.5; end
    if nargin < 2; beta = 0; end
    
    % Reshape
    x = x(:);
    x_tilde = x;
    
    switch order
        
        % The heaviside projection x_tilde
        case 'zero'
            % Eq. 21 in Xu 2010
            x_tilde(x<=eta) = eta*(exp(-beta*(1-x(x<=eta)/eta))-(1-x(x<=eta)/eta)*exp(-beta));
            x_tilde(x>eta) = (1-eta)*(1-exp(-beta*(x(x>eta)-eta)/(1-eta))+(x(x>eta)-eta)*exp(-beta)/(1-eta))+eta;
    
        % The derivative
        % d_x_tilde_d_x_bar
        case 'derivative'
            x_tilde(x<=eta) = beta*exp(-beta*(1-x(x<=eta)/eta))+exp(-beta);
            x_tilde(x>eta)  = beta*exp(-beta*(x(x>eta)-eta)/(1-eta))+exp(-beta);
            
    end
            
    % Old implementation        
    % solution.x.physical(:) = 1 - exp(-solution.beta*solution.x.intermediate(:)) + solution.x.intermediate(:) * exp(-solution.beta);
    
end