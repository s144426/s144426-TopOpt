function x = filter_linear(x,H,Hs)
    
    % Handle input
    if nargin < 3; Hs = sum(H,2); end
    
    % Reshape 
    x = x(:);
    
    % Apply linear filter
    x = (H*x)./Hs;

end

