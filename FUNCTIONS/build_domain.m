function domain = build_domain(study,domain)
    
    % Coordinates for bounding box
    domain.X = [ 0 domain.L(1) ];    % xmin xmax
    domain.Y = [ 0 domain.L(2) ];    % ymin ymax
    
    % Relaxation parameter in BC
    domain.relax = min(domain.L/domain.resolution)/5;
    
    % New mesh
    domain.newmesh = true;

    % Problem specific
    switch study.problem
        
        case 'MBB'
            domain = update_bc(domain,'Fy',-1,'point',[domain.X(1) domain.Y(2)*28/30]);
            domain = update_bc(domain,'u' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
            domain = update_bc(domain,'v' , 0,'point',[domain.X(2) domain.Y(1)]);
    
            domain.box      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(2) ];
            domain.fix      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
            domain.fd       = @(p) drectangle(p,domain.X(1),domain.X(2),domain.Y(1),domain.Y(2));
            domain.fh       = @huniform;
            
        case 'multiload'
            domain = update_bc(domain,'Fy', 1,'point',[domain.X(2) domain.Y(2)],1);
            domain = update_bc(domain,'Fy',-1,'point',[domain.X(2) domain.Y(1)],2);
            domain = update_bc(domain,'u' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
            domain = update_bc(domain,'v' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);

            domain.box      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(2) ];
            domain.fix      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
            domain.fd       = @(p) drectangle(p,domain.X(1),domain.X(2),domain.Y(1),domain.Y(2));
            domain.fh       = @huniform;
            
       case 'beam'
            domain = update_bc(domain,'Fy',-1,'point',[domain.X(2) domain.Y(2)]);
            domain = update_bc(domain,'u' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
            domain = update_bc(domain,'v' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
        
            domain.box      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(2) ];
            domain.fix      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
            domain.fd       = @(p) drectangle(p,domain.X(1),domain.X(2),domain.Y(1),domain.Y(2));
            domain.fh       = @huniform;
            
        case 'mechanism'
            domain = update_bc(domain,'Fx', 1,'point',[domain.X(1) domain.Y(2)]);
            domain = update_bc(domain,'u',  0,'point',[domain.X(1) domain.Y(1)]);
            domain = update_bc(domain,'v',  0,'point',[domain.X(1) domain.Y(1)]);
            domain = update_bc(domain,'spring_in_x',  0,'point',[domain.X(1) domain.Y(2)]);
            domain = update_bc(domain,'spring_out_x', 0,'point',[domain.X(2) domain.Y(2)]);
            domain = update_bc(domain,'v' , 0,'line' ,[domain.X(1) domain.Y(2);domain.X(2) domain.Y(2)]);
            
            domain.box      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(2) ];
            domain.fix      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
            domain.fd       = @(p) drectangle(p,domain.X(1),domain.X(2),domain.Y(1),domain.Y(2));
            domain.fh       = @huniform;
            
        case 'hole'
            domain.radius = 0.2;  % Hole radius
            domain = update_bc(domain,'Fy',-1,'point',[domain.X(2) domain.Y(2)]);
            domain = update_bc(domain,'u' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
            domain = update_bc(domain,'v' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
            
            domain.box      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(2) ];
            domain.fix      = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
            domain.fd       = @(p) ddiff(...
                                        drectangle(p,domain.X(1),domain.X(2),domain.Y(1),domain.Y(2)),...
                                        dcircle(p,domain.L(1)/4,domain.L(2)/2,domain.radius)...
                                   );
            domain.fh       = @huniform;

        case 'strange'
            
            r = domain.X(2)/20;
            
            domain = update_bc(domain,'Fy',-1,'point',[domain.L(1)*3/4 domain.L(2)*1/4]);
            domain = update_bc(domain,'u' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
            domain = update_bc(domain,'v' , 0,'line' ,[domain.X(1) domain.Y(1);domain.X(1) domain.Y(2)]);
            
            domain.box = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(2) ];
            domain.fix = [ domain.X(1) domain.Y(1) ; domain.X(2) domain.Y(1) ; domain.X(2) domain.Y(2) ; domain.X(1) domain.Y(2) ];
            domain.fd  = @(p)  dunion(...
                                    drectangle(p,0,domain.L(1)*1/2,0,domain.L(2)),...
                                    dunion(...
                                        drectangle(p,0,domain.L(1)*3/4,0,domain.L(2)/2),...
                                        dcircle(p,domain.L(1)*3/4,domain.L(2)*1/4,domain.L(2)*1/4)...
                                    ),...
                                    ddiff(...
                                        drectangle(p,domain.L(1)/2,domain.L(1)/2+r,domain.L(2)/2,domain.L(2)/2+r),...
                                        dcircle(p,domain.L(1)/2+r,domain.L(2)/2+r,r)...
                                    )...
                                );
            domain.fh  = @huniform;


    end
    
end