
% RUN_FEA Solves a Finite Element problem.

function solution = run_fea(solution,study,domain,mesh)

    % Prepare
    A = mesh.K(mesh.support.freedofs,mesh.support.freedofs);
    b = mesh.load.F(mesh.support.freedofs,:);

    % Solve
    solution.U(mesh.support.freedofs,:) = numsolve(A,b);
    
    % Give me the deflection of the initial distribution
    maxu = full(mean(solution.U(abs(solution.U)==max(max(abs(solution.U))))));
    
    
    switch study.type
        
        case 'fea'
            
            fprintf('Max U:\t %7.3e\n',maxu);
            
        case 'topopt'
        
            % Update maxu history
            % Initialised in build_variables
            solution.history.maxu = [solution.history.maxu maxu];

            % Lagrangian problem (adjoint load case)
            if isfield(mesh,'mechanism')
                free = mesh.support.freedofs;
                solution.G = sparse(mesh.mechanism.l.out,ones(length(mesh.mechanism.l.out),1),1,mesh.nn*2,1);
                solution.lagrangian = zeros(mesh.nn*2,1);
                solution.lagrangian(free) = mesh.K(free,free)\-solution.G(free);
            end
    
    end
    
    % Update plot
    update_plot(solution,study,domain,mesh,'deflection');
          
    
end