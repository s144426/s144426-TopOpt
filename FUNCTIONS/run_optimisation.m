function solution = run_optimisation(solution,study,domain,mesh)
    
    % Run optimisation algorithm
    timer = tic;
    switch study.optimisation
        case 'oc'  , solution =  run_oc(solution,study,mesh);
        case 'mma' , solution = run_mma(solution,study,mesh);
    end
    solution.time.optimise = solution.time.optimise + toc(timer);
    
    % Print to terminal
    if study.plot.terminal
    fprintf('iter: %3i %2i, obj: %-5.2f, v: %-4.2f, chng: %-5.3f, beta: %-3.1f, simp: %-3.1f\n',...
        solution.loop.iter,solution.loop.mma,solution.c,mean(solution.x.physical(:)),solution.change,solution.beta,solution.simp);
    end
    
    % Update iteration
    solution.loop.iter = solution.loop.iter + 1;
    
    % Update simp loop
    solution.loop.simp = solution.loop.simp + 1;
    
    % Update beta loop
    solution.loop.beta = solution.loop.beta + 1;
    
    % Update change history
    % Initialised in build_variables
    solution.history.change = [solution.history.change solution.change];

    % Update SIMP
    if solution.simp < study.simp.stop 
        if solution.change(end) <= study.stop || solution.loop.simp >= study.simp.iter
            solution.simp = min( solution.simp + study.simp.grow , study.simp.stop );
            solution.change(end) = min(study.move,study.stop*1e3);
            solution.loop.simp = 0;
            solution.loop.beta = 0;
            solution.loop.mma = 0;
        end
    else
        if solution.beta < study.beta.stop          % Beta not done
        if solution.loop.simp >= study.simp.iter    % Sufficient iterations later
        if strcmp(study.filter.type,'heaviside')    % Heaviside filter
        if solution.change(end) <= study.stop || solution.loop.beta >= study.beta.iter
            if solution.beta == 0
                solution.beta = solution.beta + 1;
            else
                solution.beta = solution.beta * study.beta.grow;
            end
            solution.change(end) = min(study.move,100*study.stop);
            solution.loop.beta = 0;
            solution.loop.mma = 0;
        end
        end
        end
        end
    end
    
    % Plot the current design variable and info
    update_plot(solution,study,domain,mesh,'design');
    
    % Plot the current deflection
    update_plot(solution,study,domain,mesh,'deflection');
    
    % Plot the history
    update_plot(solution,study,domain,mesh,'history');
    
    % Evaluate convergence
    solution.converged = ...
        solution.loop.iter > study.iterations || (...
        solution.change < study.stop && ...
        solution.simp >= study.simp.stop && ...
        ( solution.beta >= study.beta.stop || ~strcmp(study.filter.type,'heaviside') ) );
    
end

function solution = run_oc(solution,study,mesh)

    damping = 0.5;
    if strcmp(study.problem,'mechanism'); damping = 0.3; end
    
    l1 = 0; 
    l2 = 1e7; 
    move = study.move;
    x = solution.x.design;
    dc = solution.dc;
    dv = solution.dv;

    while (l2-l1)/(l1+l2) > 1e-4
        
        lmid = (l2+l1)/2;
        solution.x.design = ...
            max(0,max(x-move,min(1,min(x+move,x.*(max(1e-10,-dc./lmid)).^damping))));

        % Filter design to physical
        solution = sub_filter(solution,study,mesh);

        total_volume = study.volfrac*mesh.ne;

        if sum(solution.x.physical(:)) > total_volume
            l1 = lmid; 
        else
            l2 = lmid; 
        end
    end

    % How much did we change
    solution.change = max( abs(solution.x.design(:)-x(:)) );
    
end

function solution = run_mma(solution,study,mesh)

    % Update counter
    solution.mma.iter = solution.loop.mma;
    
    % Update mma history
    solution.mma.xold2 = solution.mma.xold1;
    solution.mma.xold1 = solution.mma.x;
    solution.mma.x = solution.x.design(:);

    % Update min max values
    solution.mma.xmin = max(0,solution.mma.x-study.move);
    solution.mma.xmax = min(1,solution.mma.x+study.move);
    
    % Update objective function
    solution.mma.f0   = solution.c / solution.history.objective(1);
    solution.mma.df0  = solution.dc(:) ./ solution.history.objective(1);
    
    % Update constraint function
    solution.mma.f1   = solution.v;
    solution.mma.df1  = solution.dv(:);
    
    % Call mmasolve
    [solution.x.design,~,~,~,~,~,~,~,~,solution.mma.lower,solution.mma.upper] = mmasolve(solution.mma);
    
    % How much did we change
    solution.change = max(abs(  solution.x.design(:)-solution.mma.x(:)  ));

    % Filter design to physical
    solution = sub_filter(solution,study,mesh);   
    
    % Update mma iteration
    solution.loop.mma  = solution.loop.mma  + 1;
    
end

function solution = sub_filter(solution,study,mesh)

    % Line 74-78 in top88
    % Filter design to physical
    switch study.filter.type
        case 'none'
            solution.x.physical(:)      = solution.x.design(:);
        case 'sensitivity'
            solution.x.physical(:)      = solution.x.design(:);
        case 'density'
            solution.x.physical(:)      = filter_linear(solution.x.design,mesh.H,mesh.Hs);
        case 'heaviside'
            solution.x.intermediate(:)  = filter_linear(solution.x.design,mesh.H,mesh.Hs);
            solution.x.physical(:)      = filter_heaviside(solution.x.intermediate,solution.beta,study.eta);
            
        otherwise
            error('Incorrectly given input variable study.filter...');
    end
    

end