function solution = study_topopt(study,domain,material)

    % Print start message
    fprintf([repmat('=',1,48) '\nTopology optimisation running...\n' repmat('=',1,48) '\n'])
    
    % Clear figs
    close all force
    
    % Parallel
    if ~isfield(study,'parallel'); study.parallel = false; end
    if study.parallel && isempty(gcp('nocreate'))
        parpool(4)
    end
    
    % Start new analysis
    solution.new  = true;
    mesh.new      = true;
    
    % Prepare iteration
    solution.time.total = tic;
    solution.time.optimise = 0;
    solution.converged = false;
    
    % Iterate to convergence
    while ~solution.converged
        
        % Initialise solution 
        solution = build_initial(solution,study);
        
        % Build mesh
        mesh = build_mesh(solution,study,domain,material,mesh);
        
        % Build solution variables
        solution = build_variables(solution,study,mesh);
        
        % Build stiffness
        mesh = build_stiffness(solution,study,mesh,material);
                
        % Finite Element Analysis
        solution = run_fea(solution,study,domain,mesh);
        
        % Objective function and its sensitivity
        solution = run_objective(solution,study,mesh,material);
        
        % Finite difference check of sensitivities
        solution = run_check(solution,study,domain,mesh,material);
        
        % Constraints
        solution = run_constraints(solution,study,mesh);
        
        % Filtering
        solution = run_filter(solution,study,mesh);
        
        % Update design variables (Optimality criteria for now)
        solution = run_optimisation(solution,study,domain,mesh);
        
    end
    
    % Print end message
    solution.time.total = toc(solution.time.total);
    solution.dofs = numel(mesh.nodes);
    solution.eles = mesh.ne;
    fprintf([repmat('-',1,48) '\nConverged in %2f s\n' repmat('=',1,48) '\n'],solution.time)
    
    % Plot and export final design
    update_plot(solution,study,domain,mesh,'final');  
    
end