
n = 400;
A = 200;
a = zeros(1,n);

% Seq
tic
for i = 1:n
    a(i) = max(abs(eig(rand(A))));
end
toc

% Par
tic
parfor i = 1:n
    a(i) = max(abs(eig(rand(A))));
end
toc