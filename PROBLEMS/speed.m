% Introduction to Topology Optimisation
% Niels Brummerstedt, DTU, 2019

% Start
close all force, clear variables, clear global
addpath ARCHIVE FUNCTIONS FUNCTIONS/FEA FUNCTIONS/SOLVERS FUNCTIONS/HELPERS ...
    FUNCTIONS/MESHING FUNCTIONS/MESHING/DISTMESH OUTPUT PROBLEMS

% Init
res = [20:2:30 35:5:50 60:10:90];
res = [100:10:140];
opt = {'oc','mma'};
M = zeros(length(res),length(opt)*3);

for i = 1:length(res)
    for j = 1:length(opt)
        
        % Clear 
        clear study
        clear material
        clear domain
        clear solution
        
        % Study parameters
        study.type          = 'topopt';
        study.name          = 'test';
        study.problem       = 'MBB';      % MBB, multiload, beam, mechanism
        study.domain        = 'box';            % box, hole, skew
        study.meshtype      = 'quick';       	% quick (top88), simple (top88ext), noisy, distmesh (triangular), exmesh (quad)
        study.optimisation  = opt{j};             % oc, mma
        study.filter.type   = 'density';        % none, sensitivity, density, heaviside
        study.filter.size   = 10/100;
        study.volfrac       = 30/100;
        study.simp.start    = 3;
        study.simp.iter     = 1;
        study.simp.grow     = 1/20;
        study.simp.stop     = 3;
        study.beta.start    = 0;
        study.beta.iter     = 20;
        study.beta.grow     = 2;
        study.beta.stop     = 256;
        study.eta           = 1;
        study.stop          = 1/20;
        study.iterations    = 500;
        study.move          = 0.2;
        study.checkfd       = 0;

        % Plots
        study.plot.terminal = false;
        study.plot.mesh     = false;
        study.plot.boundary = false;
        study.plot.design   = false;
        study.plot.deflect  = false;
        study.plot.history  = false;
        study.plot.filter   = false;
        study.plot.final    = false;
        study.plot.finalbc  = false;
        study.plot.save     = false;

        % Materials
        material.E0         = 1;
        material.Emin       = 1e-9;
        material.nu         = 0.3;
        material.model      = 'simp';           % simp, real

        % Domain
        domain.resolution   = res(i);                           % Number of elements on shortest side
        study.filter.size   = domain.resolution*study.filter.size;
        domain.L            = [2 1]*domain.resolution;      % Lengths
        domain              = build_domain(study,domain);   % Problem definition

        % Run the analysis
        switch study.type
            case 'fea'   , solution = study_fea(    study, domain, material  );
            case 'topopt', solution = study_topopt( study, domain, material  );
        end

        M(i,j*3-0) = solution.time.optimise;
        M(i,j*3-1) = solution.dofs;
        M(i,j*3-2) = solution.eles;
        
    end
end
dlmwrite('OUTPUT/speed_extension.txt',M,'\t');
    
